#!/bin/bash
function statusCheck(){
	if [ `sudo netstat -putona | grep "$1"| grep ":$2 "| grep "LISTEN" | wc -l` == 0 ]
	then
		echo "Servicio $1 en el puerto $2 caido" | mail -s "Servicio Caido" user@dominio.com
	fi
}

statusCheck nginx 80
statusCheck nginx 443
statusCheck mysqld 3306