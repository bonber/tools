#!/bin/bash

TOKEN=""
CHAT_ID=""
URL="https://api.telegram.org/bot$TOKEN/sendMessage"

function statusCheck(){
	if [ `sudo netstat -putona | grep "$1"| grep ":$2 "| grep "LISTEN" | wc -l` == 0 ]
	then
		curl -s -X POST $URL -d chat_id=$CHAT_ID -d text="[ $HOSTNAME:$2 ] - $1 caido"
	fi
}

statusCheck apache2 80
statusCheck apache2 443
statusCheck python 1212
statusCheck python 6565
statusCheck nginx 8081
